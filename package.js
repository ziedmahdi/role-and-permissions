Package.describe({
  name: 'ziedmahdi:role-and-permissions',
  version: '0.2.0',
  // Brief, one-line summary of the package.
  summary: 'ACL System based on roles and permissions',
  // URL to the Git repository containing the source code for this package.
  git: 'https://bitbucket.org/ziedmahdi/role-and-permissions',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: null
});

Package.onUse(function(api) {
  api.use('tap:i18n@1.7.0');
  api.addFiles("package-tap.i18n", ["client", "server"]);
  api.use('check@1.1.0');
  api.use('mongo@1.1.3');
  api.use(['accounts-password@1.1.4', 'templating@1.1.5', 'tracker@1.0.9']);
  api.addFiles(['templates/showRole.html', 'templates/showRole.js', 'templates/createNewRole.html', 'templates/createNewRole.js' ], 'client');
  api.addFiles(['templates/editRole.html', 'templates/editRole.js'], 'client');
  api.addFiles(['i18n/en.i18n.json'], ["client", "server"]);
  api.addFiles('role-and-permissions.js');
  api.export('Roles');
});

Package.onTest(function(api) {
  api.use('tinytest');
  api.use('ziedmahdi:role-and-permissions');
  api.addFiles('role-and-permissions-tests.js');
});
