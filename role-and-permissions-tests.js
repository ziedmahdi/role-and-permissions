// Write your tests here!
// Here is an example.
/* global Roles */

Meteor.methods({
    add: function(id, perm){
        Roles.addPermissionToUser(id, perm);
    },

    remove: function(id, perm){
        Roles.removeUserPermission(id, perm);
    },

    insert: function(){

    }
});

Tinytest.add('add permissions', function (test) {
    if ( Meteor.isServer){
        //empty the users collection for test purpose
        Meteor.users.remove({});

        var user = {
            name: "zied"
        };
        var perm = { user: [ 'delete' ] };
        var permString = 'user.delete';

        var perms = {
            user: [ 'create', 'update', 'show' ],
            role: [ 'delete', 'update', 'show' ]
        };
        var permsString = ['user.create', 'user.update', 'user.show', 'role.delete', 'role.update', 'role.show']

        var id = Meteor.users.insert(user);

        var currentUser;

//         Roles.addPermissionToUser( id, perm);
         currentUser = Meteor.users.findOne(id);
//         test.equal(Roles.userCan(perm, currentUser), true, 'adding single permission to useris not working');

//         Roles.addPermissionToUser( id, perms);
//         currentUser = Meteor.users.findOne(id);
//         test.equal(Roles.userCan(perm, currentUser), true, 'adding single permission to useris not working after adding multiple permissions');
//         test.equal(Roles.userCan(perms, currentUser), true, 'adding multiple permissions to user is not working');
//         perms.forEach(function(permission){
//             test.equal(Roles.userCan(permission, currentUser), true, 'adding multiple permission is not working when checking one by one');
//         });

//         Roles.removeUserPermission(id, perm);
//         currentUser = Meteor.users.findOne(id);
//         test.equal(Roles.userCan(perm, currentUser), false, 'removing single permission is not working');

//         Roles.removeUserPermission(id, perms);
//         currentUser = Meteor.users.findOne(id);
//         test.equal(Roles.userCan(perms, currentUser), false, 'removing multiple permission is not working');
//         perms.forEach(function(permission){
//             test.equal(Roles.userCan(permission, currentUser), false, 'removing multiple permission is not working when checking one by one');
//         });

//         test.equal(Roles.userCan('ziedpermission', currentUser), false, 'unknown permission is not working');

        //empty the role collection for test purpose
        Meteor.roles.remove({});


        var oldName = 'UsersManager';
        var newName = 'UserManager';

        var roleObject = {
            name: oldName,
            permissions: perm
        };

        var roleId = Roles.newRole(oldName, perm);
        var role = Meteor.roles.findOne(roleId);
        delete role._id;
        test.equal(role, roleObject, "Role is not like what we expect");

        test.throws(function(){
            Roles.newRole(oldName.toUpperCase());
        }, "already");

        test.equal(Roles.roleCan(perm, oldName), true, "Role permission is not working when affected by string");
        test.equal(Roles.roleCan(permString, oldName), true, "Role permission is not working when affected by string when checked with unwanted permission");

        Roles.setRolePermissions(oldName, perms);
        roleObject.permissions = perms;
        role = Meteor.roles.findOne(roleId);
        delete role._id;
        test.equal(role, roleObject, "After changing the role permissions, it becomes not like what we expect");

        test.equal(Roles.roleCan(perm, oldName), false, "Role permission is not working after changing the role permissions with unwanted permission");
        test.equal(Roles.roleCan(perms, oldName), true, "Role permission is not working after changing the role permissions");
        permsString.forEach(function(permission){
            test.equal(Roles.roleCan(permission, oldName), true, "Role permission is not working after changing the role permissions when checking one by one:" + permission);
        });

        test.throws(function(){
            Roles.setUserRole(id, "ziedrole");
        }, "role does not exist");

        Roles.setUserRole(id, oldName);
        currentUser = Meteor.users.findOne(id);
        test.equal(currentUser.profile.role, oldName, "Role has not been affected to user");

        test.equal(Roles.userCan(perms, currentUser), true, "User has not unherit permissions from its role");
        permsString.forEach(function(permission){
            test.equal(Roles.userCan(permission, currentUser), true, 'User has not unherit permissions from its role when checking one by one');
        });

        var secondUserId = Meteor.users.insert({name: "greg"});
        var thirdUserId = Meteor.users.insert({name: "omar"});

        Roles.setUserRole(secondUserId, oldName);
        Roles.newRole("oldName", perm);
        Roles.setUserRole(thirdUserId, "oldName");


        Roles.changeRoleName(oldName, newName);
        currentUser = Meteor.users.findOne(id);
        var secondUser = Meteor.users.findOne(secondUserId);
        var thirdUser = Meteor.users.findOne(thirdUserId);
        role = Meteor.roles.findOne(roleId);
        delete role._id;
        roleObject.name = newName;
        test.equal(role, roleObject, "After changing the role name, it becomes not like what we expect");
        test.equal(currentUser.profile.role, newName, "After changing the role name, the associated users has not been updated");
        test.equal(secondUser.profile.role, newName, "After changing the role name, the second associated user has not been updated");
        test.equal(thirdUser.profile.role, "oldName", "After changing the role name, unrelated user has been updated");

//         Roles.addPermissionToRole(newName, perm);
//         test.equal(Roles.roleCan(perm, newName), true, "After adding a single permission to role, permissions are different then expected");
//         test.equal(Roles.roleCan(perms, newName), true, "After adding a single permission to role, pervious permissions are different then expected");
//         test.equal(Roles.userCan(perm, currentUser), true, "After adding a single permission to role, user permissions are different then expected");
//         test.equal(Roles.userCan(perms, currentUser), true, "After adding a single permission to role, user pervious permissions are different then expected");

        role = Meteor.roles.findOne(roleId);
        test.throws(function(){
            Roles.deleteRole(newName);
        }, "user is associated with this role");
        test.equal(role, Meteor.roles.findOne(roleId), "Role should not be deleted if a user is associated with it");

        role = Meteor.roles.findOne(roleId);
        Meteor.users.remove(id);
        Meteor.users.remove(secondUserId);
        Roles.deleteRole(newName);
        var roleAfterDelete = Meteor.roles.findOne(roleId);
        test.equal(roleAfterDelete, undefined, "Role should be deleted if no user is associated with it");

        var defaultRole = 'Admin';
        var unmodifiableRole = "Expert";
        Roles.config({
            defaultRole: defaultRole,
            unmodifiableRoles: [unmodifiableRole],
            defaultUser: {
                profile:{
                    role: defaultRole
                },
                username: "admin",
                email: "admin@parisimmo.com",
                password: "administrator"
            },
            seedDb: true
        });

        var adminRole = Meteor.roles.findOne({name: defaultRole});
        test.isNotNull(adminRole, "Admin role was not inserted automatically");

        test.throws(function(){
            Roles.deleteRole(defaultRole);
        }, "change or delete this role");

        test.throws(function(){
            Roles.changeRoleName(defaultRole, "notGonnaHappen");
        }, "change or delete this role");

        var expertRoleId = Roles.newRole(unmodifiableRole, perm);

        test.throws(function(){
            Roles.deleteRole(unmodifiableRole);
        }, "change or delete this role");

        test.throws(function(){
            Roles.changeRoleName(unmodifiableRole, "notGonnaHappen");
        }, "change or delete this role");

        var modifiableRoleName = "modifiableRole";
        var modifiableRoleId = Roles.newRole(modifiableRoleName, {});

        var modifiableRole = Meteor.roles.findOne({name: modifiableRoleName});
        test.isNotNull(adminRole, "Modifiable role was not created");

        var expectedModifiableRole = {
            _id: modifiableRoleId,
            name: modifiableRoleName,
            permissions: {}
        };

        test.equal(modifiableRole, expectedModifiableRole, "Modifiable role is not as expected");

        var deletableRoleName = "deletableRole";
        Roles.changeRoleName( modifiableRoleName, deletableRoleName);
        modifiableRole = Meteor.roles.findOne(modifiableRoleId);
        expectedModifiableRole.name = deletableRoleName;

        test.equal(modifiableRole, expectedModifiableRole, "Modifiable role name didn't change");

        Roles.deleteRole(deletableRoleName);
        var deletableRole = Meteor.roles.findOne(modifiableRoleId);

        test.isUndefined(deletableRole, "Deletable role was not deleted");

    }
});
