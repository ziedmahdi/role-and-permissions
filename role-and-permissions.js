/**
 *  Copyright (C) 2015 I.B.S.S. All Rights Reserved.
 */

/* global Roles: true */


//TODO: add simple-schema to validate inputs (e.g perms must be an array containing objects )


/**
 * The Roles collection
 * @type {Mongo.Collection}
 */
Meteor.roles = new Mongo.Collection('roles');


Roles = {

    /**
     * For package general configuration
     */
    configuration:{
        lang: {lng: 'en'},
        permissions: {}
    },


    /**
     * Checks if the role has a permission
     * @param perms single or multiple permissions
     * @param role
     * @return {boolean}
     */
    roleCan: function (perms, role) {
        if ( role ){
            if (typeof role == 'string'){
                role = Meteor.roles.findOne({name: role});
                if ( !role ){
                    return false;
                }
            }

            if ( !role.permissions ){
                return false;
            }


            if ( typeof perms == 'string' ){
                //e.g user.create
                var tempArray = perms.split('.');
                if (tempArray.length === 2){
                    perms = {};
                    perms[tempArray[0]] = [tempArray[1]];
                } else {
                    throw new Meteor.Error(TAPi18n.__('role.invalid_permissions_given.error', this.configuration.lang), TAPi18n.__('role.invalid_permissions_given.reason', this.configuration.lang));
                }
            } else {
                //TODO: check if perms is valid
            }

            //get the global permissions that we are looking for
            var wantedGlobalPermissions = Object.keys(perms);

            for (var i = 0; i < wantedGlobalPermissions.length; ++i){
                var rolePossibleActionForPermission = role.permissions[wantedGlobalPermissions[i]];

                if ( rolePossibleActionForPermission ){
                    //role has this global permission,
                    //let's see if role can perform the wanted actions
                    var specifiedActions = perms[wantedGlobalPermissions[i]];

                    for( var j = 0; j < specifiedActions.length; ++j ){
                        if ( rolePossibleActionForPermission.indexOf(specifiedActions[j]) == -1 ){
                            return false
                        }
                    }

                } else {
                    return false;
                }
            }

            return true;
        } else {
            throw new Meteor.Error(TAPi18n.__('role.invalid_role_name.error', this.configuration.lang), TAPi18n.__('role.invalid_role_name.reason', this.configuration.lang));
        }
    },


    /**
     * Checks if the user has the permission
     * @param perms
     * @param {Object | String} user the user object or the userId
     * @return {boolean}
     */
    userCan: function (perms, user) {

        if ( typeof user == 'string' ){
            user = Meteor.users.findOne(user);
        }

        if ( !user ){
            return false;
        }
//         var can = false;

//         //check the user permissions first
//         if ( user.permissions ){
//             for (var i = 0; i < perms.length; ++i){
//                 if ( user.permissions.indexOf(perms[i]) == -1 ){
//                     can = false;
//                     break;
//                 } else {
//                     can = true;
//                 }
//             }
//         }

//         if ( can ){
//             return true;
//         } else {
        //check the user's role permissions
        if ( user.profile ){
            var userRole = Meteor.roles.findOne({name: user.profile.role});
            if ( userRole ) {
                return this.roleCan(perms, userRole);
            }
        }

        return false;
//         }
    },

    loggedInUserCan: function (perms){
        var loggedInUser = Meteor.user();

        if ( loggedInUser ){
            return this.userCan(perms, loggedInUser);
        } else {
            return false;
        }
    },

    /**
     * Checks if the given role name is a valid role
     * @param roleName
     */
    isRoleValid: function(roleName){
        // Check if role exists
        if ( Meteor.roles.findOne({name: { $regex : new RegExp(roleName, "i")}}) ) {
            return true;
        } else {
            return false;
        }
    }
};

if ( Meteor.isServer ){

    /**
     * For package private configuration
     */
    Roles.configuration.seedDb = false;

    Roles.configuration.defaultRole = 'Admin';

    Roles.configuration.defaultUser = {
        profile:{
            firstName: "Admin",
            lastName: "Admin",
            role: 'Admin'
        },
        username: "admin",
        email: "admin@admin.com",
        password: "adminstrator"
    };

    Roles.configuration.unmodifiableRoles = [];

    /**
     * Set the package configuration
     * @param configuration
     */
    Roles.config = function(configuration){
        if ( configuration.lang ){
            check(configuration.lang, String);
            this.configuration.lang = {lng: configuration.lang};
        }

        if (( configuration.defaultRole ) && ( configuration.defaultUser)){
            this.configuration.defaultRole = configuration.defaultRole;

            this.configuration.defaultUser = configuration.defaultUser;
            this.configuration.defaultUser.profile.role = this.configuration.defaultRole;
        }

        if ( configuration.unmodifiableRoles ){
            if ( Array.isArray(configuration.unmodifiableRoles) ){
                this.configuration.unmodifiableRoles = configuration.unmodifiableRoles;
            } else {
                throw new Meteor.Error(TAPi18n.__('role.invalid_unmodifiable_roles.error', this.configuration.lang), TAPi18n.__('role.invalid_unmodifiable_roles.reason', this.configuration.lang));
            }
        }

        if ( configuration.permissions ){
            this.configuration.permissions = configuration.permissions;
        }

        if ( configuration.seedDb ) {
            this.configuration.seedDb = configuration.seedDb;

            if ((!Meteor.roles.findOne({name: this.configuration.defaultRole})) && ( this.configuration.seedDb )) {

                this.newRole(this.configuration.defaultRole, this.configuration.permissions);

                //inject an default user
                if ( this.configuration.defaultUser ) {
                    var defaultUserId = Accounts.createUser(this.configuration.defaultUser);

                    //make the user verified
                    Meteor.users.update(defaultUserId, {$set: {"emails.0.verified": true}});
                }
            }
        }
    };

    /**
     * Create a new role
     * Only from server side
     * @param name
     * @param {Object} permissions
     */
    Roles.newRole = function (name, permissions) {
        check(name, String);
        name = name.trim();

        //check if name is an empty string
        if ( name == "" ){
            throw new Meteor.Error(TAPi18n.__('role.empty_role_name.error', this.configuration.lang), TAPi18n.__('role.empty_role_name.reason', this.configuration.lang));
        }

        //check if there is a role with the same name
        if ( this.isRoleValid(name) ){
            throw new Meteor.Error(TAPi18n.__('role.role_already_exists.error', this.configuration.lang), TAPi18n.__('role.role_already_exists.reason', this.configuration.lang));
        }

        var role = {
            name: name
        };

        if ( permissions ){
            //TODO: validate that permission are as exwpected
            role.permissions = permissions;
        } else {
            role.permissions = {};
        }

        return Meteor.roles.insert(role);
    };

    /**
     * Delete a given role if
     * Only from server side
     * @param name
     */
    Roles.deleteRole = function ( name ) {
        //check that the system authorize the modification of the name of this role
        if (( name != this.configuration.defaultRole ) && ( this.configuration.unmodifiableRoles.indexOf(name) == -1 )){
            //check that role doesn't contains any user
            if ( Meteor.users.findOne({ "profile.role" : name }) ){
                throw new Meteor.Error(TAPi18n.__('role.role_has_user.error', this.configuration.lang), TAPi18n.__('role.role_has_user.reason', this.configuration.lang));
            } else {
                Meteor.roles.remove({ name: name });
            }
        } else {
            throw new Meteor.Error(TAPi18n.__('role.role_is_unmodifiable.error', this.configuration.lang), TAPi18n.__('role.role_is_unmodifiable.reason', this.configuration.lang));
        }
    };

    /**
     * Edit role name
     * Only from server side
     * @param oldName
     * @param newName
     */
    Roles.changeRoleName = function ( oldName, newName ) {
        //check that new name is not empty
        newName = newName.trim();

        if ( newName == "" ){
            throw new Meteor.Error(TAPi18n.__('role.empty_role_name.error', this.configuration.lang), TAPi18n.__('role.empty_role_name.reason', this.configuration.lang));
        }

        //check that new name is different from the old one
        if ( oldName == newName ){
            return;
        }

        //check that the system authorize the modification of the name of this role
        if (( oldName != this.configuration.defaultRole ) && ( this.configuration.unmodifiableRoles.indexOf(oldName) == -1 )){
            //check if there is a role with the same name
            if ( this.isRoleValid(newName) ){
                throw new Meteor.Error(TAPi18n.__('role.role_already_exists.error', this.configuration.lang), TAPi18n.__('role.role_already_exists.reason', this.configuration.lang));
            }

            var numberOfAffectedDocs = Meteor.roles.update({ name: oldName }, {
                $set: {
                    name: newName
                }
            });

            return this.updateRelatedUsers(oldName, newName);
        } else {
            throw new Meteor.Error(TAPi18n.__('role.role_is_unmodifiable.error', this.configuration.lang), TAPi18n.__('role.role_is_unmodifiable.reason', this.configuration.lang));
        }

    };

    /**
     * Set role permissions
     * Only from server side
     * @param roleName
     * @param {[Object]}  perms
     */
    Roles.setRolePermissions = function (roleName, perms) {
        //TODO: validate permissions to be like schema
        return Meteor.roles.update({ name: roleName }, {
            $set: { permissions: perms }
        });
    };

//     /**
//      * Add single or multiple permissions to a given role
//      * Only from server side
//      * @param roleName
//      * @param {Array} | {String} perms
//      */
//     Roles.addPermissionToRole = function (roleName, perms) {
//         if ( Array.isArray(perms) ){

//         }
//         var permissions = typeof perms == 'string' ? { permissions: perms } : { permissions: { $each: perms } };

//         return Meteor.roles.update({ name: roleName }, {
//             $push: permissions
//         });
//     };

//     /**
//      * Remove permission from a  given role.
//      * Only from server side
//      * @param roleName
//      * @param {Array} | {String} perms
//      */
//     Roles.removeRolePermission = function (roleName, perms) {
//         var permissions = typeof perms == 'string' ? { permissions: perms} : { permissions: { $in: perms }};

//         return Meteor.roles.update({ name: roleName }, {
//             $pull: permissions
//         });
//     };


    /**
     * Sets the user's role
     * Only from server side
     * @param userId
     * @param roleName
     */
    Roles.setUserRole = function (userId, roleName) {
        if (this.isRoleValid(roleName)){
            return Meteor.users.update(userId, {
                $set: {"profile.role": roleName}
            });
        } else {
            throw new Meteor.Error(TAPi18n.__('role.role_not_found.error', this.configuration.lang), TAPi18n.__('role.role_not_found.reason', this.configuration.lang));
        }
    };

    /**
     * Update all the users within a role when updating
     * Only from server side, private method
     * @param oldRoleName
     * @param newRoleName
     */
    Roles.updateRelatedUsers = function (oldRoleName, newRoleName) {
        return Meteor.users.update({ "profile.role": oldRoleName }, {
            $set: {"profile.role": newRoleName}
        }, {multi: true});
    };

//     /**
//      * Add single or multiple permissions to a given user
//      * Only from server side
//      * @param userId
//      * @param {[Object]} | {Object} perms
//      */
//     Roles.addPermissionToUser = function (userId, perms) {
//         check(userId, String);

//         var permissions = Array.isArray(perms) ? { permissions: perms } : { permissions: { $each: perms } };

//         return Meteor.users.update(userId, {
//             $push: permissions
//         });
//     };

//     /**
//      * Remove permission from a user.
//      * If the user's role has this permission, nothing is done.
//      * @param userId
//      * @param {Array} | {String} perms
//      */
//      Roles.removeUserPermission = function (userId, perms) {
//         check(userId, String);

//         var permissions = typeof perms == 'string' ? { permissions: perms} : { permissions: { $in: perms }};

//         Meteor.users.update(userId, {
//             $pull: permissions
//         });
//     };
}

if (Meteor.isClient) {
    /**
     * Extract the role data from form
     * Helper when creating a new role, or updating an existing role.
     * @returns {{name: String, permissions: {}}}
     */
    Roles.extractRoleDataFromForm = function(){
        var permissions = {};
        $('select.tag-select:not(.show-only)').each(function(){
            var select = $(this);
            var value = select.val();
            var globalPermission = select.attr('id');
            if ( value ){
                permissions[globalPermission] = value;
            }
        });

        var name = $('input#name').val();

        return {
            name: name,
            permissions: permissions
        };
    };

    /**
     * Returns the role of the current user
     * @return {String}
     */
    Meteor.role = function () {
        var user = Meteor.user();

        if ( user ) {
            if ( user.profile ){
                return user.profile.role;
            }
        } else {
            return "guest";
        }
    };


    /**
     * Subscribes to role when user logs in or is modified (potentially his role)
     */
    Tracker.autorun(function () {
        var loggedInUser = Meteor.user();

        if ( loggedInUser ) {
            var subscription = Meteor.subscribe('userRole', loggedInUser.profile.role);

            //Get the permissions from server
            Meteor.call('getSystemPermissions', function(error, result){
                if ( !error ){
                    console.log("permissions received on the client");
                    Roles.configuration.permissions = result;
                }
            });
        }
    });

    /**
     * Checks if the current user has the permission
     */
    Template.registerHelper('userCan', function (perms) {
        return Roles.loggedInUserCan(perms);
    });
}

if (Meteor.isServer) {

    /**
     * Publish the role of the current user
     */
    Meteor.publish('userRole', function (roleName) {
        if ( this.userId ) {
            //get the user role
            var loggedInUser = Meteor.users.findOne(this.userId, {fields: {"profile.role": 1}});
            return Meteor.roles.find({name: loggedInUser.profile.role});
        }
        return this.ready();
    });

    Meteor.methods({
        getSystemPermissions: function(){
            console.log('get permissiosn is called');
            var currentUser = Meteor.user();

            if ( currentUser ) {
                if (( Roles.userCan("role.create", currentUser) ) || ( Roles.userCan("role.edit", currentUser) )){
                    return Roles.configuration.permissions;
                }
            }

            return {};
        }
    });
}