Template.EditRoleForm.helpers({
    roleCan: function(){
        if ( Roles.roleCan(Template.parentData(1) + '.' + this, Template.parentData(2).role)){
            return 'selected';
        }
    },

    globalPermissions: function(){
        return Object.keys(Roles.configuration.permissions);
    },

    selectName: function(){
        return this + '[]';
    },

    actions: function(){
        return Roles.configuration.permissions[this];
    },

    nameForI18n: function(){
        return "role." + this;
    }
});