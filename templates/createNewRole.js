Template.NewRoleForm.helpers({
    globalPermissions: function(){
        return Object.keys(Roles.configuration.permissions);
    },

    selectName: function(){
        return this + '[]';
    },

    nameForI18n: function(){
        return "role." + this;
    },

    actions: function(){
        return Roles.configuration.permissions[this];
    }
});