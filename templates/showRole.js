Template.ShowRole.helpers({
    globalPermissions: function(){
        return Object.keys(this.role.permissions);
    },

    hasGlobalPermissions: function(){
        return Object.keys(this.role.permissions).length > 0;
    },

    actions: function(){
        return Template.parentData(1).role.permissions[this];
    },

    nameForI18n: function(){
        return "role." + this;
    },
});